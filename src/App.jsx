import { useState } from "react";
import "./App.css";

function App() {
  let  [dice, setDice] = useState({dice1:1,dice2:1});

  
let update = ()=>{
  
  let dice1=(( Math.floor(Math.random() * 10)%6)+1)
  let dice2=((Math.floor(Math.random() * 10)%6)+1)
  setDice({dice1,dice2})
}

let topPlayer =()=>{
  if(dice.dice1 > dice.dice2){
    return "Player 1 wins"
    }else if(dice.dice2 > dice.dice1){
      return "Player 2 wins"
      }else{  
        return "It's a draw"
        }
}

  return (
    <div className="bg-black text-red-600 py-10">
      <h1 className="text-3xl font-bold p-5">{topPlayer()}</h1>
      <div className="flex justify-center gap-28 text-2xl font-bold p-5">
      <h1>Player1</h1>
      <h1>Player2</h1>
      </div>
      <div className="container flex justify-center">
        <img className="dice1 w-[200px] h-[200px] transform transition duration-300 hover:rotate-90"src={`${dice.dice1}.png`} alt="Dice img"/>
       
        <img className="dice2 w-[200px] h-[200px] transform transition duration-300 hover:rotate-90"  src={`${dice.dice2}.png`} alt="Dice img"/>
      </div>
        <button onClick={update} className="bg-black  px-4 py-2 rounded-lg">play</button>
    </div>
  );
}

export default App;
